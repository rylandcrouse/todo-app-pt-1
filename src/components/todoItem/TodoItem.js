import React from "react";

export default function ToDoItem(props) {
    let { toggleComplete, todoDelete, id } = props;

    function handleToggle() {
        toggleComplete(id)
    }

    function handleDelete() {
        todoDelete(id)
    }

    return (
        <li className={props.completed ? "completed" : ""}>
            <div className="view">
                <input className="toggle" type="checkbox" defaultChecked={props.completed} onClick={handleToggle} />
                <label>{props.title}</label>
                <button className="destroy" onClick={handleDelete} />
            </div>
        </li>
    )
}