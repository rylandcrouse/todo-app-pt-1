import React from 'react';
import TodoItem from '../todoItem/TodoItem'

export default function TodoList(props) {
    let { toggleComplete, todoDelete } = props
    return (
        <section className="main">
            <ul className="todo-list">
                {props.todos.map((todo) => (
                    <TodoItem key={todo.id} title={todo.title} completed={todo.completed} toggleComplete={toggleComplete} todoDelete={todoDelete} id={todo.id} />
                ))}
            </ul>
        </section>
    )
}