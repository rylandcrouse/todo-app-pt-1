import React, { useState, useEffect } from "react";
import todosList from "./todos.json";
import TodoList from "./components/todoList/TodoList"
import { v4 as uuidv4 } from 'uuid';

function App() {
  let [todos, setTodos] = useState(todosList)

  function addTodo(e) {
    e.preventDefault();
    let todo = {
      completed: false,
      id: uuidv4(),
      title: e.target.newTodo.value,
      userId: 1,
    }
    setTodos([todo, ...todos])
    e.target.newTodo.value = "";
    console.log(todo)
  }

  useEffect(() => {
    console.log(todos)
  }, [])

  let toggleComplete = (id) => {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo,
            completed: !todo.completed
          }
        }
        return todo;
      })
    )
  }

  function todoDelete(id) {
    setTodos(todos.filter(todo => todo.id !== id))
  }

  function clearCompleted() {
    setTodos(todos.filter(todo => !todo.completed))
  }


  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <form onSubmit={addTodo}>
          <input name="newTodo" className="new-todo" placeholder="What needs to be done?" autoFocus />
        </form>
      </header>
      <TodoList todos={todos} addTodo={addTodo} toggleComplete={toggleComplete} todoDelete={todoDelete} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
          </span>
        <button className="clear-completed" onClick={clearCompleted}>Clear completed</button>
      </footer>
    </section>
  );
}

export default App;
